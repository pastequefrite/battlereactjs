import React, { Component } from 'react';
import './GameView.scss';

class GameView extends Component {

  render() {
    return (
      <div className="GameView">
        <h1>Guerrier : {this.props.guerrierLvl}</h1>
        <div className="allhp">
            <div className="hpj1"><h1>Hp : {this.props.hpJoueur1}</h1></div>
            <div className="hpj2"><h1>Hp : {this.props.hpJoueur2}</h1></div>
        </div>
          <h2>{this.props.infoAction}</h2>
      </div>
    );
  }
}

export default GameView;
