import React, { Component } from 'react';
import Button from '@material-ui/core/Button';

import './Actions.scss';

export default class Actions extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="Actions">
                <Button variant="contained" color="primary" className="Button ButtonBlue" onClick={() => this.props.action("Attaquer")}>
                    Attaquer
                </Button>
                <Button variant="contained" color="primary" className="Button ButtonRed" onClick={() =>this.props.action("Parer")}>
                    Parer
                </Button>
                <Button variant="contained" color="primary" alt="icoStar" className="Button ButtonGrey" onClick={() => this.props.action("Charger")}>
                    Charger
                </Button>
            </div>
        );
    }
}