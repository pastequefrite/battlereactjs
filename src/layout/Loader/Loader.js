import React, { Component } from 'react';
import './Loader.scss';
import logo from "../../logo.png";
import Button from '@material-ui/core/Button';
import { Redirect } from 'react-router-dom';

export default class Loader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            redirect: false
        };
    }

    handleSubmit = () => {
        this.setState({ redirect: true });
    };

    render() {
        const { redirect } = this.state;

        if (redirect) {
            return <Redirect to="/game"/>;
        }

        return (
            <div className="Loader">
                <img src={logo} className="Logo" alt="logo" />
                <h1>Battle ReactJS</h1>
                <Button variant="contained" color="primary" className="Button" onClick={this.handleSubmit}>
                    Jouer
                </Button>
            </div>
        );
    }

}